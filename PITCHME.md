## 全チームサマリー

<canvas class="stretch" data-chart="bar">
<!-- 
{ 
 "data" : {
  "labels" : ["FirstPenguin", "Oh!366", "Talk or Treat"],
  "datasets" : [{
        "label" : "人件費",
        "borderColor" : "#2ecc71",
        "backgroundColor" : "#2ecc71",
        "data" : [579890, 786540, 282150]
        },
        {
        "label" : "出金",
        "borderColor" : "#3498db",
        "backgroundColor" : "#3498db",
        "data" : [17170, 199992, 61565]
        },
        {
        "type" : "line",
        "label" : "人件費上限",
        "borderColor" : "rgba(255, 0, 0, 0.5)",
        "backgroundColor" : "rgba(255, 0, 0, 0.5)",
        "data" : [1000000, 1000000, 1000000],
        "fill" : false
        },
        {
        "type" : "line",
        "label" : "出金上限",
        "borderColor" : "rgba(255, 0, 0, 0.5)",
        "backgroundColor" : "rgba(255, 0, 0, 0.5)",
        "data" : [200000, 200000, 200000],
        "fill" : false
        }],
    "options" : { 
        "responsive" : true
        }
    }
}
-->
</canvas>

---

## firstpenguin

<canvas class="stretch" data-chart="pie">
<!-- 
{ 
 "data" : {
  "labels" : ["Alexa Echo","残り"],
  "datasets" : [{
      "backgroundColor" : [
        "#2ecc71",
        "#3498db",
        "#9b59b6",
        "#f1c40f",
        "#e74c3c",
        "#34495e",
        "#95a5a6"
      ],
      "data" : [17170,182830]
        }]
    }
}
-->
</canvas>

---

## oh366

<canvas class="stretch" data-chart="pie">
<!-- 
{ 
 "data" : {
  "labels" : ["ノートPC","Tシャツ","合宿","o365利用料","残り"],
  "datasets" : [{
      "backgroundColor" : [
        "#2ecc71",
        "#3498db",
        "#9b59b6",
        "#f1c40f",
        "#e74c3c",
        "#34495e",
        "#95a5a6"
      ],
      "data" : [77214,12890,98688,11200,8]
        }]
    }
}
-->
</canvas>

---

## talkortreat

<canvas class="stretch" data-chart="pie">
<!-- 
{ 
 "data" : {
  "labels" : ["人感センサー","ジャンプワイヤー","タブレット","アーム","Wifi（お試し）","Wifi保険（お試し）","ラズパイ","貯金箱","ジャンプワイヤ","Wifi+保険（長期）","残り"],
  "datasets" : [{
      "backgroundColor" : [
        "#2ecc71",
        "#3498db",
        "#9b59b6",
        "#f1c40f",
        "#e74c3c",
        "#34495e",
        "#95a5a6"
      ],
      "data" : [684,467,12800,1299,5070,900,6980,2980,385,30000,138435]
        }]
    }
}
-->
</canvas>

